package com.example.hp.collegehub;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hp.collegehub.webservice.ApiClient;
import com.example.hp.collegehub.webservice.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewScannedDocuments extends AppCompatActivity {
    TextView scan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_scanned_documents);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        scan = findViewById(R.id.astatus);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String cdate = df.format(c);
        final SharedPreferences sp = getSharedPreferences("scanval", Context.MODE_PRIVATE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<registerresponse> call = apiService.getDoc("MarkAttendence", sp.getString("sid", ""), sp.getString("sname", "")
                , sp.getString("did", ""), sp.getString("cid", ""), sp.getString("sem", ""), cdate, "Present");
        call.enqueue(new Callback<registerresponse>() {
            @Override
            public void onResponse(Call<registerresponse> call, Response<registerresponse> response) {
                //Toast.makeText(ViewScannedDocuments.this, response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                scan.setText(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<registerresponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t + "", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), ScanCam.class));
        finish();
    }
}
