package com.example.hp.collegehub.webservice;



import com.example.hp.collegehub.registerresponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("Attendencemarker.jsp")
    Call<registerresponse> getDoc(@Query("key") String getDocument, @Query("sid") String scan3,@Query("sname") String scan
    ,@Query("did") String scan1,@Query("cid") String scan2,@Query("sem") String scan7,@Query("cdate") String scan8
    ,@Query("status") String scan9);
}
