package com.example.hp.collegehub.qrcodereaderview;

public enum Orientation {
  PORTRAIT, LANDSCAPE
}
