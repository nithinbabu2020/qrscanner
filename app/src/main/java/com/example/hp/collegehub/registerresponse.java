package com.example.hp.collegehub;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class registerresponse {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("qrimg")
    @Expose
    private String qrimg;
    @SerializedName("docimg")
    @Expose
    private String docimg;

    public String getDocimg() {
        return docimg;
    }

    public void setDocimg(String docimg) {
        this.docimg = docimg;
    }

    public String getQrimg() {
        return qrimg;
    }

    public void setQrimg(String qrimg) {
        this.qrimg = qrimg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
